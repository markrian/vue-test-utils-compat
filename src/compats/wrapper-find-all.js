export class WrapperArray extends Array {
  constructor(results) {
    super();
    Object.assign(this, results);
    this.wrappers = results;
  }

  at(i) {
    return this[i];
  }

  trigger(...params) {
    this.wrappers.forEach((w) => w.trigger(...params));
  }

  exists() {
    return this.length > 0 && this.wrappers.every((x) => x.exists());
  }
}

export function install(VTU, config) {
  [VTU.config.plugins.DOMWrapper, VTU.config.plugins.VueWrapper].forEach((pluginHost) =>
    pluginHost.install((wrapper) => {
      const { findAll, findAllComponents } = wrapper;
      return {
        findAll: (...args) => {
          const results = findAll.call(wrapper, ...args);

          if (config.WRAPPER_FIND_BY_CSS_SELECTOR_RETURNS_COMPONENTS) {
            const componentResults = findAllComponents.call(wrapper, ...args);
            results.forEach((v, i) => {
              const matchingComponent = componentResults.find((w) => w.element === v.element);
              if (matchingComponent) {
                results[i] = matchingComponent;
              }
            });
          }

          return config.WRAPPER_FIND_ALL ? new WrapperArray(results) : results;
        },
        findAllComponents: (...args) => {
          const results = findAllComponents.call(wrapper, ...args);
          return config.WRAPPER_FIND_ALL ? new WrapperArray(results) : results;
        },
      };
    })
  );
}
