import { jest, describe, beforeEach, it, expect } from "@jest/globals";
import { installCompat, compatFlags } from "../../../src/index.js";
import { describeOption } from "../../helpers.js";

describeOption(compatFlags.EXPORT_ERROR_WRAPPER, () => {
  let VTU;

  beforeEach(async () => {
    jest.resetModules();
    VTU = { ...(await import("@vue/test-utils")) };
  });

  describe("when enabled", () => {
    beforeEach(() => {
      installCompat(VTU, { [compatFlags.EXPORT_ERROR_WRAPPER]: true });
    });

    it("exposes ErrorWrapper on VTU module", () => {
      expect(VTU.ErrorWrapper).toBeInstanceOf(Function);
    });
  });

  describe("when disabled", () => {
    beforeEach(() => {
      installCompat(VTU, { [compatFlags.EXPORT_ERROR_WRAPPER]: false });
    });

    it("does not expose ErrorWrapper", () => {
      expect(VTU.ErrorWrapper).toBeUndefined();
    });
  });
});
