import { jest, describe, afterEach, beforeEach, it, expect } from "@jest/globals";
import { defineComponent, h } from "vue";
import { installCompat, compatFlags } from "../../../src/index.js";
import { describeOption } from "../../helpers.js";

describeOption(compatFlags.WRAPPER_UNWRAP_PROPS, () => {
  let VTU;

  const FakeComponent = defineComponent({
    props: ["demo"],
    render() {
      return h("div", this.demo);
    },
  });

  beforeEach(async () => {
    jest.resetModules();
    VTU = await import("@vue/test-utils");
  });

  let wrapper;

  afterEach(() => {
    wrapper.unmount();
  });

  const REFERENCE_PROP = {
    foo: "bar",
  };

  describe("when enabled", () => {
    beforeEach(() => {
      installCompat(VTU, { [compatFlags.WRAPPER_UNWRAP_PROPS]: true });
      wrapper = VTU.mount(FakeComponent, { props: { demo: REFERENCE_PROP } });
    });

    it("should return unwrapped props when enabled", async () => {
      expect(wrapper.props().demo).toBe(REFERENCE_PROP);
      expect(wrapper.props("demo")).toBe(REFERENCE_PROP);
    });
  });

  describe("when disabled", () => {
    beforeEach(() => {
      installCompat(VTU, { [compatFlags.WRAPPER_UNWRAP_PROPS]: false });
      wrapper = VTU.mount(FakeComponent, { props: { demo: REFERENCE_PROP } });
    });

    it("should return wrapped props when enabled", async () => {
      expect(wrapper.props().demo).not.toBe(REFERENCE_PROP);
      expect(wrapper.props().demo).toStrictEqual(REFERENCE_PROP);
      expect(wrapper.props("demo")).not.toBe(REFERENCE_PROP);
      expect(wrapper.props("demo")).toStrictEqual(REFERENCE_PROP);
    });
  });
});
