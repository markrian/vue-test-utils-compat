import { jest, describe, afterEach, beforeEach, it, expect } from "@jest/globals";
import { h } from "vue";
import { installCompat, compatFlags } from "../../../src/index.js";
import { describeOption } from "../../helpers.js";

describeOption(compatFlags.WRAPPER_ATTRIBUTES_FROM_ATTRS, () => {
  let VTU;

  const FakeComponent = {
    inheritAttrs: false,
    render() {
      return h("input", { type: "text", disabled: "" });
    },
  };

  beforeEach(async () => {
    jest.resetModules();
    VTU = await import("@vue/test-utils");
  });

  let wrapper;

  afterEach(() => {
    wrapper.unmount();
  });

  describe("when enabled", () => {
    beforeEach(() => {
      installCompat(VTU, { [compatFlags.WRAPPER_ATTRIBUTES_FROM_ATTRS]: true });
      wrapper = VTU.mount(FakeComponent, { props: { extra: "foo" } });
    });

    it("should return value from $attrs", () => {
      expect(wrapper.attributes("extra")).toBe("foo");
    });
  });

  describe("when disabled", () => {
    beforeEach(() => {
      installCompat(VTU, { [compatFlags.WRAPPER_ATTRIBUTES_FROM_ATTRS]: false });
      wrapper = VTU.mount(FakeComponent, { props: { extra: "foo" } });
    });

    it("should not return value from $attrs", () => {
      expect(wrapper.attributes("extra")).toBeUndefined();
    });
  });
});
