import { jest, describe, afterEach, beforeEach, it, expect } from "@jest/globals";
import { defineComponent, h } from "vue";
import { installCompat, compatFlags } from "../../../src/index.js";
import { describeOption } from "../../helpers.js";

describeOption(compatFlags.WRAPPER_EXCLUDE_LISTENERS_FROM_PROPS, () => {
  let VTU;

  const FakeComponent = defineComponent({
    props: ["onClick"],
    render() {
      return h("div", "test");
    },
  });

  beforeEach(async () => {
    jest.resetModules();
    VTU = await import("@vue/test-utils");
  });

  let wrapper;

  afterEach(() => {
    wrapper.unmount();
  });

  describe("when enabled", () => {
    beforeEach(() => {
      installCompat(VTU, { [compatFlags.WRAPPER_EXCLUDE_LISTENERS_FROM_PROPS]: true });
      wrapper = VTU.mount(FakeComponent, { props: { demo: "test", onClick() {} } });
    });

    it("should not include event listeners by default", async () => {
      expect(wrapper.props()).not.toHaveProperty("onClick");
    });
  });

  describe("when disabled", () => {
    beforeEach(() => {
      installCompat(VTU, { [compatFlags.WRAPPER_EXCLUDE_LISTENERS_FROM_PROPS]: false });
      wrapper = VTU.mount(FakeComponent, { props: { demo: "test" }, onClick() {} });
    });

    it("should include event listeners by default", async () => {
      expect(wrapper.props()).toHaveProperty("onClick");
    });
  });
});
