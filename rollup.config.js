import { defineConfig } from "rollup";

export default defineConfig({
  input: ["src/index.js"],
  output: [
    {
      format: "cjs",
      file: "dist/vue-test-utils-compat.cjs.js",
    },
    {
      format: "es",
      file: "dist/vue-test-utils-compat.mjs.js",
    },
  ],
});
